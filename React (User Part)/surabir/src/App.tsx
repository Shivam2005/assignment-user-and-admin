import React from 'react';
import logo from './logo.svg';
import './App.css';
import {BrowserRouter as Router,Link,Route} from "react-router-dom"
import Login from "./components/Login"
import Success from "./components/Success"


const App = () => {
  return (
    <Router>
    <main>

    <Route path="/" exact component={Login} />
    <Route path="/success" exact component={Success} />

    </main>
</Router>
  )
}

export default App;
