import React from 'react'
import '../styles/Fooditem.css';

function Fooditem(props : any) {
    //console.log(props);
    const id : number = props.item;
    let qty : number = props.qty || 0;
    const itemname: string = props.itemName;
    let price : number = props.price;
  const addOne = () => props.updateQty(id, qty + 1);
  console.log(props)
  const subtractOne = () => props.updateQty(id, qty - 1);
  return (
    <div className="CartItem">
        <div>{id}</div>
      <div>{itemname}</div>
      <div>{price} Rs</div>
      <div>
        <button onClick={subtractOne} disabled={qty <= 0}>
          -
        </button>
        {qty}
        <button onClick={addOne}>+</button>
      </div>
      <div>Cost: {qty * price} Rs</div>
    </div>
  )
}
export default Fooditem
