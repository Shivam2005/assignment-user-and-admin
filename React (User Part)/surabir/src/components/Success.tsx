import {getMenu} from "../services/menuservice"
import Menu from "../model/menu"
import { useEffect,useState } from "react"
import Fooditem from "./Fooditem"
import '../styles/FoodMenu.css';
import { saveBill } from "../services/userservice";
import { Bill } from "../model/bill";

const Success = (props : any) => {
    //console.log(props.location.state.name);
    const [loadingData, setLoadingData] = useState(true);
    const [menuData, setMenuData] = useState([]);
    //menuData : Menu[] = [];

    const initialState = props.location.state.items || sessionStorage.getItem("items");
    const [items, setItems] = useState(initialState);
    const [grandTotal,setGrandTotal] = useState(0);



     useEffect(() => {
        var temp = 0;
        items.map((item : any) => {
            if (item.qty) {
                temp += item.qty * item.price;
            }
            console.log(temp);
        });
        console.log('hellooooooooooo')
        setGrandTotal(temp);
    });
    const updateQty = (id: number, newQty : number) => {
        const newItems = items.map((item : any) => {
            if (item.item === id) {
                return { ...item, qty: newQty };
            }
            return item;
        });
        //console.log(newItems)
        setItems(newItems);
        
        
    };
    const handleLogout = () => {
        let selectedItems : any = [];
        for (let i in items) {
            if (items[i].qty > 0){
                for (let k = 0; k < items[i].qty; k++) {
                    selectedItems.push(items[i].item);
                }
            }
        } 
        if (grandTotal > 0) {
            saveBill(selectedItems).then(() => {
                alert('Your Bill is saved Successfully...');
                props.history.push('/')
            },(error) => {
                console.log('Please try again...');
            })
        }
        else {
            props.history.push('/')
        }
    }

   


    return (
        <div style={{padding : "5px 5px 5px 5px"}}>
            <button style={{textAlign:"center",margin:"1% 40%"}}className = "logout-btn" onClick = {handleLogout}>Sign Out</button>
            <div className="Cart">
                <h1 style={{color:"brown"}} className="Cart-title">Surabi Restaurant Menu Items</h1>
                <div style={{color:"brown"}} className = "CartItem">
                    <div><strong>Item id</strong></div>
                    <div><strong>Item Name</strong></div>
                    <div><strong>Item Cost</strong></div>
                    <div><strong>Item Qty</strong></div>
                    <div><strong>Total Cost</strong></div>
                </div>
                <div style={{color:"brown"}} className="Cart-items">
                    {items.map((item : any)=> (
                        <Fooditem key={item.item} updateQty={updateQty} {...item} />
                    ))}
                </div>
                <h3 style={{color:"brown", textAlign:"center"}} className="Bill">Total Payable Cost: {grandTotal} Rs</h3>
            </div>
        </div>
    )
}
export default Success;