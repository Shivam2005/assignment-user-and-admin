
import { useEffect, useState} from 'react';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';

import TextField from '@material-ui/core/TextField';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import CardHeader from '@material-ui/core/CardHeader';
import Button from '@material-ui/core/Button';
import User from "../model/user"
import {getAll} from "../services/userservice"
import { registerUser } from '../services/userservice';
import {getMenu} from "../services/menuservice"

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    container: {
      width: 1000,
      margin: `${theme.spacing(0)} auto`
    },
    loginBtn: {
      marginTop: theme.spacing(1),
      flexGrow: 0.7
    },
    regBtn: {
        marginTop: theme.spacing(2),
        flexGrow: 0.7
    },
    header: {
      textAlign: 'center',
      background: 'brown',
      color: '#fff'
    },
    card: {
      marginTop: theme.spacing(5)
    }
  })
);
const Login = (props: any) => {

    const classes = useStyles();

    const [name,setName] = useState("");
    const [password,setPassword] = useState("");

    const handleOnChange = (event : any) => {
        event.preventDefault();
        const nameProp = event.target.name;
        const value = event.target.value;
        if (nameProp === 'name') {
            setName(event.target.value);
        }
        else
        setPassword(event.target.value);
    }

    const handleRegister = () => {
        let myUser = new User()
        myUser.name = name;
        myUser.password = password;
        getAll().then((data) => {
            let currentUser = data.filter((us : User) => {
                return myUser.name === us.name && myUser.password === us.password;
            })
        
            if (currentUser.length > 0) {
                alert('You are a registered user...'); 
            }
            else {
                registerUser(myUser).then(() => {
                    alert('Registered Successfully...');
                    setName('');
                    setPassword('');   
                })
            }
        })
    }
    const handleLogin = () => {
        let myUser = new User()
        myUser.name = name;
        myUser.password = password;
        getAll().then((data) => {
            console.log(data);
            
            let currentUser = data.filter((us : User) => {
                return myUser.name === us.name && myUser.password === us.password;
              })
        
              if (currentUser.length > 0) {
                    getMenu().then((data) => {
                        sessionStorage.setItem("items",data);
                        props.history.push('/success',{name : myUser.name,items : data})
                    })
                    
              }
              else {
                alert("Please try again...");
              }
        })

    }

    return (

      <>
      <h1 style={{color: "brown", textAlign:"center"}}>Welcome to Surabi Amisha...</h1>
        <form className={classes.container} noValidate autoComplete="off">
      <Card className={classes.card}>
        <CardHeader className={classes.header} title="User Login" />
        <CardContent>
          <div>
            <TextField
              
              fullWidth
              id="username"
              type="email"
              label="User Name"
              name = "name"
              placeholder="User Name"
              margin="normal"
              onChange = {handleOnChange}
            />
            <TextField
              
              fullWidth
              id="password"
              type="password"
              name = "password"
              label="User Password"
              placeholder="User Password"
              margin="normal"
              onChange = {handleOnChange}
            />
          </div>
        </CardContent>
        <CardActions>
          <Button
            variant="outlined"
            size="medium"
            className={classes.loginBtn}
            onClick={handleLogin}
            >
            Sign in
          </Button>
          <Button
            variant="outlined"
            size="medium"
            className={classes.regBtn}
            onClick={handleRegister}
            >
            Sign up
          </Button>
        </CardActions>
      </Card>
    </form>
    </>
    );
}
export default Login;