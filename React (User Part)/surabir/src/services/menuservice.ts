export const getMenu = async () => {
    const response = await fetch("http://localhost:8080/surabi/user/menu")
    return await response.json();
}