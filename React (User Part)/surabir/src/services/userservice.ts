import {Bill} from "../model/bill"
import User from "../model/user"
export const getAll = async () => {
    const response = await fetch("http://localhost:8080/surabi/admin/users")
    return await response.json();
}
export const saveBill = async (bill : []) => {
    const response = fetch('http://localhost:8080/surabi/user/order', {
        method: 'POST', // or 'PUT'
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(bill),
    });
    //.then(response => response.json());
    return (await response).json
}
export  const registerUser = async (user : User) => {
    const response = fetch('http://localhost:8080/surabi/user/register', {
        method: 'POST', // or 'PUT'
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(user),
    });
    //.then(response => response.json());
    return (await response).json
}