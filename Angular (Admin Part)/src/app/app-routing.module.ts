import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {LoginComponent} from './components/login/login.component';
import { AdminsuccessComponent } from './components/adminsuccess/adminsuccess.component';
import { UserlistComponent } from './components/userlist/userlist.component';
import { UsercreateComponent } from './components/usercreate/usercreate.component';

const routes: Routes = [
  { path: 'success', component: AdminsuccessComponent },
  {path : '', component : LoginComponent},
  {path: 'users', component: UserlistComponent},
  { path: 'create', component: UsercreateComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
