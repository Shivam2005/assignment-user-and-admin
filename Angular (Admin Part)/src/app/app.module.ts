import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';


import { CustomMaterialModule } from './core/material.module';
import {FormsModule} from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import { AdminsuccessComponent } from './components/adminsuccess/adminsuccess.component';
import { HeaderComponent } from './components/header/header.component';
import { UsercreateComponent } from './components/usercreate/usercreate.component';
import { UserlistComponent } from './components/userlist/userlist.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    AdminsuccessComponent,
    HeaderComponent,
    UsercreateComponent,
    UserlistComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    CustomMaterialModule,
    FormsModule,
    BrowserAnimationsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
