import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Bill } from '../model/bill';

@Injectable({
  providedIn: 'root'
})
export class BillserviceService {

  constructor(private http : HttpClient) { }
  getAllBills(){
    return this.http.get<Bill[]>('http://localhost:8080/surabi/admin/bills');
  }

  getCurrMonthSale() {
    const data =  this.http.get('http://localhost:8080/surabi/admin/sale',{responseType : 'text'})
    console.log(data);
    return data;
  }
}
