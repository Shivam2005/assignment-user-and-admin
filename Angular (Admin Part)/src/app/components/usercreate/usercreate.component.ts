import { HttpHeaders } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AdminService } from 'src/app/services/admin.service';

@Component({
  selector: 'app-usercreate',
  templateUrl: './usercreate.component.html',
  styleUrls: ['./usercreate.component.css']
})
export class UsercreateComponent implements OnInit {

  user = {
    id : '',
    name: '',
    password: '',
  };
  submitted = false;
  constructor(private adminService : AdminService,private router : Router) { }

  ngOnInit(): void {
    if (history.state.data)
    this.user = history.state.data;
  }

  newData = {}
  createUser(): void {
    let data = {
      name: this.user.name,
      password: this.user.password
    };
    // checking if admin is updating user if yes then calling update method else calling create method
    if (history.state.data) {
      this.newData = {id : history.state.data.id,...data}
      this.adminService.update(this.newData)
      .subscribe(
        response => {
          console.log(response);
          alert('User updated Successfully...')
          this.router.navigate(['/users']);
        },
        error => {
          console.log(error);
        });
    }
    else {
        this.adminService.create(data)
          .subscribe(
            response => {
              console.log(response);
              this.submitted = true;
            },
            error => {
              console.log(error);
            });
      }
  }

  newUser(): void {
    this.submitted = false;
    this.user = {
      id : '',
      name: '',
      password: ''
    };
  }

  redirectToSuccess() {
    this.router.navigate(['success']);
  }

}
