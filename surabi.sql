create database surabi;

use surabi;

/*
	Creating a table users which will store id,username and password for the user
*/
CREATE TABLE users (
    id int not null AUTO_INCREMENT,
    name varchar(255),
    password varchar(255),
	PRIMARY key (id)
);

/*
	Inserting the data into the user table
*/
INSERT INTO Users (id,name, password)
VALUES (1,"shivam","shivam"),
(2,"amisha","amisha"),
(3,"me","me");


/*
	Creating a table Admin which will store id,username and password for the Admin
*/
CREATE TABLE Admin (
    ID int not null,
    Name varchar(255),
    Password varchar(255),
	PRIMARY key (ID)
);


/*
	Inserting the data into the admin table
*/

INSERT INTO Admin (ID,Name,Password)
VALUES (5,"admin","admin"),
(6,"accolite","accolite"),
(7,"hr","hr");



/*
	Creating a table RestaurantMenu which will item no, price and dishName that user wants to order
*/

CREATE TABLE RestaurantMenu (
    item int not null,
    dishname varchar(255),
    Price double,
	PRIMARY key (item)
);


/*
	Inserting the data into the restaurantmenu table
*/

INSERT INTO RestaurantMenu (item,dishname,Price)
VALUES (1,"Samosa",20),
(2,"Poha",15),
(3,"Kachori",10),
(4,"Burger",50),
(5,"Noodles",60),
(6,"Idli",50),
(7,"Pizza",130),
(8,"vada paav",30),
(9,"Dosa",80),
(10,"Cake",230);

/*
	Creating a table BillSummary which will current month,current date and price of the order
*/

CREATE TABLE BillSummary (
	srno int not null AUTO_INCREMENT,
    month VARCHAR(255),
    date varchar(255),
    Price double,
	PRIMARY key(srno)
);
