package com.greatlearning.surabiassign3.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import com.greatlearning.surabiassign3.entity.User;
import com.greatlearning.surabiassign3.entity.RestaurantMenu;



public interface UserDAO {
	
	public User getById(int id);
	public List<RestaurantMenu> showMenu();
	public String takeOrder(int []items);
	public String save(User user);
	public List<User> findAll();
}
