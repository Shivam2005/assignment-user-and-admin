package com.greatlearning.surabiassign3.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.greatlearning.surabiassign3.entity.User;
import com.greatlearning.surabiassign3.entity.RestaurantMenu;

public interface UserService {
	
	public boolean login(int id , String name , String password) ;
	public void logout() ;
	public List<RestaurantMenu> attendUser(User customer) ;
	
	public User getById(int id);
	public List<RestaurantMenu> showMenu();
	public String takeOrder(int []items);
	public String save(User user);
	public List<User> findAll();
}
