package com.greatlearning.surabiassign3.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.greatlearning.surabiassign3.entity.Admin;
import com.greatlearning.surabiassign3.entity.Bill;
import com.greatlearning.surabiassign3.entity.User;
import com.greatlearning.surabiassign3.service.AdminService;
import com.greatlearning.surabiassign3.service.UserService;

@RestController
@RequestMapping("/surabi/admin")
@CrossOrigin(origins = {"http://localhost:3000", "http://localhost:4200"})
public class AdminController {
	
	@Autowired
	private AdminService adminService;
	boolean loginCheck = true;
	
	
	@PostMapping("/login")
	public ResponseEntity<Map<String,String>> login(@RequestBody Admin admin) {
		
		Map<String,String> map = new HashMap<String,String>();
		if (loginCheck) {
			map.put("Error", "One admin already logged in");
			return new ResponseEntity<>(map,HttpStatus.METHOD_NOT_ALLOWED);
		}
		
		Admin getAdmin = adminService.getById(admin.getId());
		if (getAdmin == null) {
			map.put("Error", "Can not find admin by given credentials!");
			return new ResponseEntity<>(map,HttpStatus.NOT_FOUND);
		}
		if(admin.getId()==getAdmin.getId() && admin.getName().equals(getAdmin.getName()) && admin.getPassword().equals(getAdmin.getPassword())) {
			map.put("Message", "Successful Login as an admin!"
					+ "          Use @GetMapping admin/sale to show total Sale of this month"
					+ "          Use @GetMapping admin/bills  to show all bills generated today");
			loginCheck = true;
			return new ResponseEntity<>(map,HttpStatus.OK);
		}
		else{
			map.put("Error", "Can not find admin by given credentials!");
		}
		return new ResponseEntity<>(map,HttpStatus.NOT_FOUND);
	}
	
	
	
	@GetMapping("/logout")
	public ResponseEntity<Map<String,String>> logout() {
		
		Map<String,String> map = new HashMap<String,String>();
		if (loginCheck == true) {
			loginCheck = false;
			map.put("Message", "You have successfully logged out");
		}
		else {
			map.put("Message", "You are already logged out!!");
		}
		return new ResponseEntity<>(map,HttpStatus.OK);
	}
	
	@GetMapping("/users")
	public List<User> findAllUsers() {
		return adminService.findAllUsers();
	}
	
	@GetMapping
	public List<Admin> findAll(){
		return adminService.findAllAdmins();
	}
	
	
	@GetMapping("/sale")
	public String showTotalSaleForMonth() {
		if (loginCheck) return adminService.showTotalSaleForThisMonth();	
		return "Please login first" ;
	}

	@GetMapping("/bills")
	public List<Bill> showAllBillsForToday() {
		if (loginCheck) { 
			System.out.println("inside bills");
			return adminService.showAllBillsGeneratedToday();
		}
		return null;
	}
	
	
	
	@GetMapping("/user")
	public String showAPIinfo() {
		return " Use @GetMapping /user/{id} to get the user with {id}. " + " "
				+ "Use @DeleteMapping /user to delete the user.  "
				+ "Use @PostMapping /user to add the user.  "
				+ "Use @PutMapping /user to update the user.  ";
				
	}
	
	
	//// CRUD operations on user by admin 
	
	@PostMapping("/user")
	public String registerCustomer(@RequestBody User user) {
		if (loginCheck) {
			System.out.print(user.getName() + "sfkdsj" + user.getPassword());
			return adminService.registerUser(user);
		}
		else return "Please login first" ;
	}
	
	@DeleteMapping("/user")
	public String deleteCustomer(@RequestBody User user) {
		if (loginCheck) {
			User us = adminService.findById(user.getId());
			//checking if user is present in the database or not
			if (us != null ) {
				adminService.deleteUser(user);
				return "Successfully deleted user with id " + user.getId();
			}
			else return "No user found for id " + user.getId();
		}
		else return "Please login first" ;
	}
	
	
	@PutMapping("/user")
	public String updateCustomer(@RequestBody User user) {
		if (loginCheck) return adminService.updateUser(user);
		return "Please Login first" ;
	}
	
	
	@GetMapping("/user/{id}")
	public User getCustomerById(@PathVariable int id) {
		if (loginCheck) {
			return adminService.findById(id);
		}
		return null ;
	}
	
	
}
