package com.greatlearning.surabiassign3.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.greatlearning.surabiassign3.entity.Admin;



public interface Admins extends JpaRepository<Admin,String>{

}
