package com.greatlearning.surabiassign3.service;


import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.greatlearning.surabiassign3.dao.AdminDAO;
import com.greatlearning.surabiassign3.dao.Admins;
import com.greatlearning.surabiassign3.dao.CRUD;
import com.greatlearning.surabiassign3.dao.UserDAO;
import com.greatlearning.surabiassign3.entity.User;
import com.greatlearning.surabiassign3.entity.Admin;
import com.greatlearning.surabiassign3.entity.Bill;
import com.greatlearning.surabiassign3.entity.RestaurantMenu;

@Service
public class AdminServiceImplementation implements AdminService{
	
	@Autowired
	private AdminDAO adminDAO ;
	
	@Autowired
	private CRUD curd;
	
	@Autowired
	private Admins admins;
	
	@Override
	public Admin getById(int id) {
		Admin admin  = null;
		try {
			admin = adminDAO.getById(id);
		} catch(RuntimeException e) {
			System.err.println(e.getMessage());
		}
		return admin;
	}

	@Override
	public String showTotalSaleForThisMonth() {
		return adminDAO.showTotalSaleForThisMonth();
	}

	@Override
	public List<Bill> showAllBillsGeneratedToday() {
		return adminDAO.showAllBillsGeneratedToday();
	}

	@Override
	public String registerUser(User user) {
		return adminDAO.registerUser(user);	
	}

	@Override
	public String updateUser(User user) {
		// using inbuilt jparepository to save the user
		curd.save(user);
		return "User having id " + user.getId() + " is saved successfully!";
	}

	@Override
	public String deleteUser(User user) {
		// using inbuilt jparepository to delete the user
		curd.delete(user);
		return "User having id " + user.getId() + " is deleted!";
	}

	@Override
	public User findById(int id) {
		return adminDAO.findById(id);
	}

	@Override
	public List<Admin> findAllAdmins() {
		// TODO Auto-generated method stub
		return admins.findAll();
	}

	@Override
	public List<User> findAllUsers() {
		// TODO Auto-generated method stub
		return curd.findAll();
	}
	

}
