package com.greatlearning.surabiassign3.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.greatlearning.surabiassign3.entity.User;

public interface CRUD extends JpaRepository<User,String>{

}
