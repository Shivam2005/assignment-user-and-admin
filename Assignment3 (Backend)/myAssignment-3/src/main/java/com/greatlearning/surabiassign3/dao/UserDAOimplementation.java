package com.greatlearning.surabiassign3.dao;
import com.greatlearning.surabiassign3.entity.User;
import com.greatlearning.surabiassign3.entity.Bill;
import com.greatlearning.surabiassign3.entity.RestaurantMenu;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;


@Repository
public class UserDAOimplementation implements UserDAO{
	
	private EntityManager entityManager;
	
	@Autowired
	public UserDAOimplementation(EntityManager theEntityManager) {
		entityManager = theEntityManager;
	}
	

	// fetching the user by given id
	@Override
	public User getById(int id) {
		Session currentSession = entityManager.unwrap(Session.class);
		User user = currentSession.get(User.class,id);
		return user;
	}
	
	
	// method to print the menu
	@Override
	public List<RestaurantMenu> showMenu() {
		//get the Current Hibernate Session
		Session currentSession = entityManager.unwrap(Session.class);
		Query<RestaurantMenu> theQuery = currentSession.createQuery("from RestaurantMenu",RestaurantMenu.class);
		List<RestaurantMenu> menu = theQuery.getResultList();
		return menu;
	}

	
	// accepting the order related to a particular user_id
	@Override
	public String takeOrder(int[] items) {
		
		//initializing bill amount = 0
		double totalprice=0;
		for(int i : items) {
			Session currentSession = entityManager.unwrap(Session.class);
			RestaurantMenu menu = null;
			try {
				menu = currentSession.get(RestaurantMenu.class,i);
				totalprice += menu.getPrice();
			}catch(NullPointerException e) {
				// if wrong values detected in input array
				return "Wrong input values detected!! Please select correct items no.";
			}
			
		}
		String date = java.time.LocalDate.now().toString();
		
		Date date2= new Date();
		Calendar cal = Calendar.getInstance();
		cal.setTime(date2);
		int month = cal.get(Calendar.MONTH) + 1;
		
		Bill bill=new Bill();
		bill.setMonth(month);
		bill.setDate(date);
		bill.setPrice(totalprice);
		Session currentSession = entityManager.unwrap(Session.class);
		
		// storing the bill in the billsummary table
		currentSession.save(bill);
		return "Your total bill is " + Double.toString(totalprice);
	}



	



	@Override
	public String save(User user) {
		Session currentSession = entityManager.unwrap(Session.class);
		currentSession.save(user);
		return "Successfully registered!!";
	}


	@Override
	public List<User> findAll() {
		Session currentSession = entityManager.unwrap(Session.class);
		Query<User> theQuery = currentSession.createQuery("from users",User.class);
		List<User> users = theQuery.getResultList();
		return users;
	}


}
