package com.greatlearning.surabiassign3.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import com.greatlearning.surabiassign3.entity.User;
import com.greatlearning.surabiassign3.entity.Admin;
import com.greatlearning.surabiassign3.entity.Bill;
import com.greatlearning.surabiassign3.entity.RestaurantMenu;



public interface AdminDAO {
	
	public String showTotalSaleForThisMonth();
	public List<Bill> showAllBillsGeneratedToday();
	public Admin getById(int id);
	public String registerUser(User user);
	public User findById(int id);
	public List<Admin> findAll();
}
