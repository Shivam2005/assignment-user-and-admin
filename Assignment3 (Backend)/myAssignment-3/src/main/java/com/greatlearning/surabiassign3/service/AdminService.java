package com.greatlearning.surabiassign3.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.greatlearning.surabiassign3.entity.User;
import com.greatlearning.surabiassign3.entity.Admin;
import com.greatlearning.surabiassign3.entity.Bill;
import com.greatlearning.surabiassign3.entity.RestaurantMenu;

public interface AdminService {
	public Admin getById(int id);
	public String showTotalSaleForThisMonth();
	public List<Bill> showAllBillsGeneratedToday();
	public String registerUser(User user);
	public String updateUser(User user);
	public String deleteUser(User user);
	public User findById(int id);
	public List<Admin> findAllAdmins();
	public List<User> findAllUsers();
}
