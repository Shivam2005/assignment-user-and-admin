package com.greatlearning.surabiassign3;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Surabiassign3Application {

	public static void main(String[] args) {
		SpringApplication.run(Surabiassign3Application.class, args);
	}

}
