package com.greatlearning.surabiassign3.service;


import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.greatlearning.surabiassign3.dao.UserDAO;
import com.greatlearning.surabiassign3.entity.User;

import com.greatlearning.surabiassign3.entity.RestaurantMenu;

@Service
public class UserServiceImplementation implements UserService{
	
	@Autowired
	UserDAO userDAO ;
	
	@Override
	public boolean login(int id , String name , String password) {
		// TODO Auto-generated method stub
		//return user.login(id,name,password);
		return true;
	}

	@Override
	public void logout() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<RestaurantMenu> attendUser(User customer) {
		return new ArrayList<RestaurantMenu>();
	}

	@Override
	public User getById(int id) {
		User user  = null;
		try {
			user = userDAO.getById(id);
		} catch(RuntimeException e) {
			System.err.println(e.getMessage());
		}
		return user;
	}

	@Override
	public List<RestaurantMenu> showMenu() {
		return userDAO.showMenu();
	}

	@Override
	public String takeOrder(int[] items) {
		return userDAO.takeOrder(items);
	}

	@Override
	public String save(User user) {
		return userDAO.save(user);
		
	}

	@Override
	public List<User> findAll() {
		// TODO Auto-generated method stub
		return userDAO.findAll();
	}

}
