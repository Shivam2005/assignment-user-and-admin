package com.greatlearning.surabiassign3.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name = "restaurantmenu")
public class RestaurantMenu {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name ="item")
	private int item;
	
	@Column(name = "dishname")
	private String itemname;
	
	@Column(name = "Price")
	private int price;
	
	public int getItem() {
		return item;
	}
	public void setItem(int item) {
		this.item = item;
	}
	
	
	public String getItemName() {
		return itemname;
	}
	public void setItemName(String itemName) {
		this.itemname = itemName;
	}
	public int getPrice() {
		return price;
	}
	public void setPrice(int price) {
		this.price = price;
	}
	
}
