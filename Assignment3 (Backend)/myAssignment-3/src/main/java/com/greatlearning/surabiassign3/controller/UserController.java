package com.greatlearning.surabiassign3.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.greatlearning.surabiassign3.entity.RestaurantMenu;
import com.greatlearning.surabiassign3.entity.User;
import com.greatlearning.surabiassign3.service.UserService;


//import com.GreatLearning.SurabiAssignment3.entity.Admin;


@RestController
@RequestMapping("/surabi/user")
@CrossOrigin(origins = "http://localhost:3000")
public class UserController {
	@Autowired
	private UserService userService;
	
	
	//global variable to check if user is logged in or not
	boolean loginCheck = true;
	
	
	//login mapping
	@PostMapping("/login")
	public ResponseEntity<Map<String,String>> login(@RequestBody User user) {
		
		Map<String,String> map = new HashMap<String,String>();
		User getUser = userService.getById(user.getId());
		if (loginCheck) {
			map.put("Error", "Another user is already logged in!");
		}
		
		else if (getUser == null) {
			map.put("Error", "Can not find user by given credentials!");
		}
		else if(user.getId()==getUser.getId() && user.getName().equals(getUser.getName()) && user.getPassword().equals(getUser.getPassword())) {
			map.put("Message", "Successful Login");
			loginCheck = true;
			return new ResponseEntity<>(map,HttpStatus.OK);
		}
		else{
			map.put("Error", "Can not find user by given credentials!");
		}
		return new ResponseEntity<>(map,HttpStatus.NOT_FOUND);
	}
	
	
	// to register new user
	@PostMapping("/register")
	public String register(@RequestBody User user) {
		return userService.save(user);
	}
	
	@GetMapping("/all")
	public List<User> findAll(){
		return userService.findAll();
	}
	
	
	// for logout
	@GetMapping("/logout")
	public ResponseEntity<Map<String,String>> logout() {
		
		Map<String,String> map = new HashMap<String,String>();
		if (loginCheck == true) {
			loginCheck = false;
			map.put("Message", "You have logged out");
		}
		else {
			map.put("Message", "You are already logged out!!");
		}
		return new ResponseEntity<>(map,HttpStatus.OK);
	}
	
	
	// to show menu only if user logged in
	
	@GetMapping("/menu")
	public ResponseEntity<List<RestaurantMenu>> showMenu(){
		List<RestaurantMenu> list = new ArrayList<>();
		if (loginCheck == true) {
			list = userService.showMenu();
			return new ResponseEntity<>(list,HttpStatus.OK);
		}
		System.err.println("User needs to login first");
		return new ResponseEntity<>(list,HttpStatus.METHOD_NOT_ALLOWED);
	}
	
	
	
	// taking the order, input items no as an array in requestbody
	@PostMapping("/order")
	public String takeOrder(@RequestBody int []items) {
		if(loginCheck == true) {
			String resp = userService.takeOrder(items);
			return resp;
		} 
		else {
			return "User must login first!!";
		}
	}
	
}
