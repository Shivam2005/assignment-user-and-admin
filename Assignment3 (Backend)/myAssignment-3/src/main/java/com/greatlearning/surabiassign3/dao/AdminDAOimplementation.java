package com.greatlearning.surabiassign3.dao;
import com.greatlearning.surabiassign3.entity.User;
import com.greatlearning.surabiassign3.entity.Admin;
import com.greatlearning.surabiassign3.entity.Bill;
import com.greatlearning.surabiassign3.entity.RestaurantMenu;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;


@Repository
public class AdminDAOimplementation implements AdminDAO{
	
	private EntityManager entityManager;
	
	@Autowired
	public AdminDAOimplementation(EntityManager theEntityManager) {
		entityManager = theEntityManager;
	}
	


	@Override
	public Admin getById(int id) {
		Session currentSession = entityManager.unwrap(Session.class);
		Admin admin = currentSession.get(Admin.class,id);
		return admin;
	}


	// method for showing current month sale
	@Override
	public String showTotalSaleForThisMonth() {
		Session currentSession = entityManager.unwrap(Session.class);
		int month=java.time.LocalDate.now().getMonthValue();
		System.out.println("Month is " + month);
		String hql = "from Bill where MONTH(date) = "+month+"";
		Query query = currentSession.createQuery(hql);
		List<Bill> results = query.list();
		if(results.size() == 0) return "No sale in this month" ;
		double sale = 0;
		// running the loop on results to fetching the price corresponding to the given key
		for(int i=0 ; i<results.size() ; i++) {
			sale += results.get(i).getPrice();
		}
		return "Current month sale is " + Double.toString(sale);
	}


	// showing current month bills 
	@Override
	public List<Bill> showAllBillsGeneratedToday() {
		Session currentSession = entityManager.unwrap(Session.class);
		int day= java.time.LocalDate.now().getDayOfMonth();
		String hql = "from Bill where DAY(date) = "+day+"";
		Query query = currentSession.createQuery(hql);
		List<Bill> results = query.list();
		return results ;
	}



	@Override
	public String registerUser(User user) {
		Session currentSession = entityManager.unwrap(Session.class);
		currentSession.save(user);
		return "Successfully registered!!";
	}




	@Override
	public User findById(int id) {
		Session currentSession = entityManager.unwrap(Session.class);
		User user = currentSession.get(User.class,id);
		return user;
	}



	@Override
	public List<Admin> findAll() {
		Session currentSession = entityManager.unwrap(Session.class);
		Query<Admin> theQuery = currentSession.createQuery("from admin",Admin.class);
		List<Admin> admins = theQuery.getResultList();
		return admins;
	}

}
